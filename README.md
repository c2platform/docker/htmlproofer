# HTMLProofer

Docker image for [HTMLProofer](https://github.com/gjtorikian/html-proofer).

## Using image

This image does not try to do any fancy except being up-to-date.


### Command line

```shell
docker run --rm -it \
  -v $(pwd):/src \
  registry.gitlab.com/c2platform/docker/htmlproofer:5.0.7 \
  --allow-hash-href --check-html --empty-alt-ignore
```


### docker-compose

```yaml
validate:
  image: registry.gitlab.com/c2platform/docker/htmlproofer:5.0.7
  command: --allow-hash-href --check-html --empty-alt-ignore
  volumes:
    - .:/src
```


### GitLab CI

```yaml
html proofer:
  stage: test
  image: registry.gitlab.com/c2platform/docker/htmlproofer:5.0.7-ci
  script:
    - |
      htmlproofer \
        --empty-alt-ignore \
        --allow-hash-href \
        --url-swap "https?\:\/\/(www\.example\.com):" \
        dist
```


## Configuration

* Work directory: `/src`
